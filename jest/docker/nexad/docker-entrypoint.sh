#!/bin/bash
set -e

mkdir -p "$NEXA_DATA"

cat << EOF > "$NEXA_DATA/nexa.conf"
printtoconsole=1
rpcpassword=password
rpcuser=alice
zmqpubhashtx=tcp://*:28332
zmqpubrawtx=tcp://*:28332
zmqpubhashblock=tcp://*:28332
zmqpubrawblock=tcp://*:28332
txindex=1
regtest=1
rpcbind=nexad
rpcallowip=0/0
debug=1
rpcbind=nexad
electrum=0
electrum.host=0.0.0.0
electrum.monitoring.host=0.0.0.0
electrum.exec=/usr/local/bin/rostrum
EOF

chown nexa:nexa "$NEXA_DATA/nexa.conf"

# ensure correct ownership and linking of data directory
# we do not update group ownership here, in case users want to mount
# a host directory and still retain access to it
chown -R nexa "$NEXA_DATA"
ln -sfn "$NEXA_DATA" /home/nexa/.nexa
chown -h nexa:nexa /home/nexa/.nexa

exec gosu nexa "$@"
