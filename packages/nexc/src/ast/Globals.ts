import { PrimitiveType, ArrayType, BytesType } from '@nexscript/utils';
import { SymbolTable, Symbol } from './SymbolTable.js';

export const NumberUnit: { [index:string] : number } = {
  SATOSHIS: 1,
  SATS: 1,
  NEXA: 100,
  MNEXA: 100000000,
  MEX: 100000000,
  SECONDS: 1,
  MINUTES: 60,
  HOURS: 3600,
  DAYS: 86400,
  WEEKS: 604800,
};

export enum GlobalFunction {
  ABS = 'abs',
  MIN = 'min',
  MAX = 'max',
  WITHIN = 'within',
  RIPEMD160 = 'ripemd160',
  SHA1 = 'sha1',
  SHA256 = 'sha256',
  HASH160 = 'hash160',
  HASH256 = 'hash256',
  CHECKSIG = 'checkSig',
  CHECKMULTISIG = 'checkMultiSig',
  CHECKDATASIG = 'checkDataSig',
  GROUP_AMOUNT_IN = 'tx.groupAmountIn',
  GROUP_AMOUNT_OUT = 'tx.groupAmountOut',
  GROUP_COUNT_IN = 'tx.groupCountIn',
  GROUP_COUNT_OUT = 'tx.groupCountOut',
  GROUP_NTH_INPUT = 'tx.groupNthInput',
  GROUP_NTH_OUTPUT = 'tx.groupNthOutput',
  ENCODE_NUMBER = 'encodeNumber',
  ENCODE_DATA = 'encodeData',
}

export const PushTxStateSpecifierMap: { [index: string]: number } = {
  [GlobalFunction.GROUP_AMOUNT_IN]: 0x07,
  [GlobalFunction.GROUP_AMOUNT_OUT]: 0x08,
  [GlobalFunction.GROUP_COUNT_IN]: 0x09,
  [GlobalFunction.GROUP_COUNT_OUT]: 0x0A,
  [GlobalFunction.GROUP_NTH_INPUT]: 0x0B,
  [GlobalFunction.GROUP_NTH_OUTPUT]: 0x0C,
};

export enum TimeOp {
  CHECK_SEQUENCE = 'tx.age',
  CHECK_LOCKTIME = 'tx.time',
}

export enum Class {
  LOCKING_BYTECODE_P2ST = 'LockingBytecodeP2ST',
  LOCKING_BYTECODE_P2PKT = 'LockingBytecodeP2PKT',
  LOCKING_BYTECODE_NULLDATA = 'LockingBytecodeNullData',
}

export enum Modifier {
  CONSTANT = 'constant',
}

export const GLOBAL_SYMBOL_TABLE = new SymbolTable();

// Classes
GLOBAL_SYMBOL_TABLE.set(
  Symbol.class(Class.LOCKING_BYTECODE_P2ST, new BytesType(), [
    new BytesType(20), new BytesType(), new BytesType(),
  ]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.class(Class.LOCKING_BYTECODE_P2PKT, new BytesType(23), [new BytesType(20)]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.class(Class.LOCKING_BYTECODE_NULLDATA, new BytesType(), [new ArrayType(new BytesType())]),
);

// Global functions
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.ABS, PrimitiveType.INT, [PrimitiveType.INT]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.MIN, PrimitiveType.INT, [PrimitiveType.INT, PrimitiveType.INT]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.MAX, PrimitiveType.INT, [PrimitiveType.INT, PrimitiveType.INT]),
);
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.WITHIN, PrimitiveType.BOOL,
  [PrimitiveType.INT, PrimitiveType.INT, PrimitiveType.INT],
));
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.RIPEMD160, new BytesType(20), [PrimitiveType.ANY]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.SHA1, new BytesType(20), [PrimitiveType.ANY]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.SHA256, new BytesType(32), [PrimitiveType.ANY]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.HASH160, new BytesType(20), [PrimitiveType.ANY]),
);
GLOBAL_SYMBOL_TABLE.set(
  Symbol.function(GlobalFunction.HASH256, new BytesType(32), [PrimitiveType.ANY]),
);
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.CHECKSIG, PrimitiveType.BOOL,
  [PrimitiveType.SIG, PrimitiveType.PUBKEY],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.CHECKMULTISIG, PrimitiveType.BOOL,
  [new ArrayType(PrimitiveType.SIG), new ArrayType(PrimitiveType.PUBKEY)],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.CHECKDATASIG, PrimitiveType.BOOL,
  [PrimitiveType.DATASIG, new BytesType(), PrimitiveType.PUBKEY],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.GROUP_AMOUNT_IN, PrimitiveType.INT,
  [new BytesType(32)],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.GROUP_AMOUNT_OUT, PrimitiveType.INT,
  [new BytesType(32)],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.GROUP_COUNT_IN, PrimitiveType.INT,
  [new BytesType(32)],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.GROUP_COUNT_OUT, PrimitiveType.INT,
  [new BytesType(32)],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.GROUP_NTH_INPUT, PrimitiveType.INT,
  [PrimitiveType.INT, new BytesType(32)],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.GROUP_NTH_OUTPUT, PrimitiveType.INT,
  [PrimitiveType.INT, new BytesType(32)],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.ENCODE_NUMBER, new BytesType(),
  [PrimitiveType.INT],
));
GLOBAL_SYMBOL_TABLE.set(Symbol.function(
  GlobalFunction.ENCODE_DATA, new BytesType(),
  [new BytesType()],
));
