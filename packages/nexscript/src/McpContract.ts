import {
  ContractArtifact, SourceArtifact, prepareMcpContract as prepareMcpParams,
} from '@nexscript/utils';
import { Argument } from './Argument.js';
import { ContractOptions, Utxo } from './interfaces.js';
import NetworkProvider from './network/NetworkProvider.js';
import { ElectrumNetworkProvider } from './network/index.js';
import { Contract } from './Contract.js';
import { Transaction } from './Transaction.js';
import { replaceDependencyArgs } from './utils.js';

export interface McpContractParameters {
  contractName: string, // name of MAST contract to execute
  functionName: string | undefined, // name of MAST contract function to execute, or undefined if there is a single function
  parameters: Argument[], // MAST contract function arguments
}

export class McpContract {
  name: string;
  address: string;
  bytecode: string;
  bytesize: number;
  opcount: number;
  contract: Contract;
  provider: NetworkProvider;
  private artifact: ContractArtifact;

  /**
   * Create new MCP contract instance
   * @param {SourceArtifact | ContractArtifact} artifact - MCP Contract artifact, see @ref compileString
   * @param {Argument[]} constructorArgs - MCP Contract constraint parameters
   * @param {NetworkProvider} provider - Network provider for network interactions
   */
  constructor(
    artifact: SourceArtifact | ContractArtifact,
    private constructorArgs: Argument[],
    private options?: Omit<ContractOptions, 'contractName'>,
  ) {
    this.provider = this.options?.provider ?? new ElectrumNetworkProvider();

    const isSourceArtifact = 'source' in artifact && 'contracts' in artifact;

    if (isSourceArtifact) {
      if (!artifact.contracts.length) {
        throw Error('Malformed artifact: empty contract list');
      }

      const contractArtifact = artifact.contracts[0];
      if (!contractArtifact.contracts.length) {
        throw Error('First contract in the compilation output does not seem like an MCP contract');
      }
      this.artifact = contractArtifact;
    } else {
      this.artifact = artifact;
    }

    if (!this.artifact.contracts.length) {
      throw Error('Empty MAST artifacts array');
    }

    // replace dependencies in the artifact
    this.artifact.contracts.forEach((contract) => replaceDependencyArgs(contract, this.options?.dependencyArgs));

    // fully define ContractArtifact with the first mast contract
    const mcpParams = prepareMcpParams(
      this.artifact.contracts[0].contractName,
      this.artifact.contracts[0].abi[0].name,
      this.artifact,
    );

    const contract = new Contract(mcpParams.artifact, constructorArgs, options);

    this.name = contract.name;
    this.address = contract.address;
    this.bytecode = contract.bytecode;
    this.bytesize = contract.bytesize;
    this.opcount = contract.opcount;
  }

  // Get MCP contract balance
  async getBalance(): Promise<bigint> {
    const utxos = await this.getUtxos();
    return utxos.reduce((acc, utxo) => acc + utxo.satoshis, 0n);
  }

  // Get MCP contract utxos
  async getUtxos(): Promise<Utxo[]> {
    return this.provider.getUtxos(this.address);
  }

  // Prepare a transaction to execute a MAST contract with given parameters
  execute({ contractName, functionName, parameters }: McpContractParameters): Transaction {
    const mastArtifact = this.artifact.contracts.find((val) => val.contractName === contractName);
    if (!mastArtifact) {
      throw Error(`Contract with name '${contractName}' not found in MCP artifact`);
    }

    const mcpParams = prepareMcpParams(contractName, functionName, this.artifact);
    const contract = new Contract(mcpParams.artifact, this.constructorArgs, this.options);
    return contract.functions.spend(
      ...mcpParams.parameterValues, // mast merkle tree path
      ...parameters.slice().reverse(), // function arguments
      ...(mastArtifact.abi.length > 1 ? [0n] : []), // function selector
      ...this.constructorArgs.slice().reverse(), // constraint arguments
      0n, // dummy argument
    );
  }
}
