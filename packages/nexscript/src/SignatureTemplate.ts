import {
  binToHex, decodePrivateKeyWif, secp256k1, SigningSerializationFlag,
} from '@bitauth/libauth';
import nexcore from 'nexcore-lib';
import { HashType, SignatureAlgorithm, Unlocker } from './interfaces.js';

export default class SignatureTemplate {
  public privateKey: Uint8Array;

  constructor(
    signer: Keypair | Uint8Array | string,
    private hashtype: HashType = HashType.SIGHASH_ALL,
    private signatureAlgorithm: SignatureAlgorithm = SignatureAlgorithm.SCHNORR,
  ) {
    if (isKeypair(signer)) {
      const wif = signer.toWIF();
      this.privateKey = decodeWif(wif);
    } else if (typeof signer === 'string') {
      this.privateKey = decodeWif(signer);
    } else {
      this.privateKey = signer;
    }
  }

  generateSignature(payload: Uint8Array, bchForkId?: boolean): Uint8Array {
    const signature = this.signatureAlgorithm === SignatureAlgorithm.SCHNORR
      ? secp256k1.signMessageHashSchnorr(this.privateKey, payload) as Uint8Array
      : secp256k1.signMessageHashDER(this.privateKey, payload) as Uint8Array;

    return Uint8Array.from([...signature, this.getHashType(bchForkId)]);
  }

  getHashType(bchForkId: boolean = true): number {
    return bchForkId ? (this.hashtype | SigningSerializationFlag.forkId) : this.hashtype;
  }

  getPublicKey(): Uint8Array {
    return secp256k1.derivePublicKeyCompressed(this.privateKey) as Uint8Array;
  }

  unlockP2PKT(): Unlocker {
    const signatureTemplate = this;

    return {
      addInput({ transaction, input }) {
        const txo = {
          txId: input.txid,
          outputIndex: input.vout,
          satoshis: Number(input.satoshis),
          address: input.address,
        } as any;

        transaction.from(txo);
      },
      signInput({
        transaction, inputIndex: index, network,
      }) {
        const nexPrivateKey = nexcore.PrivateKey(binToHex(signatureTemplate.privateKey), network);
        const signatures = transaction.inputs[index].getSignatures(transaction, nexPrivateKey, index,
          nexcore.crypto.Signature.SIGHASH_NEXA_ALL);
        transaction.inputs[index].addSignature(transaction, signatures[0], 'schnorr');
      },
    };
  }
}

// Works for nexcore-lib PrivateKey
interface Keypair {
  toWIF(): string;
}

function isKeypair(obj: any): obj is Keypair {
  return typeof obj.toWIF === 'function';
}

function decodeWif(wif: string): Uint8Array {
  const result = decodePrivateKeyWif(wif);

  if (typeof result === 'string') {
    throw new Error(result);
  }

  return result.privateKey;
}
