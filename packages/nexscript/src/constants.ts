import nexcore from 'nexcore-lib';

export const DUST_LIMIT = 546n;
export const P2PKH_OUTPUT_SIZE = 34;
export const P2PKT_OUTPUT_SIZE = 33; // 1 + 8 + 1 + 23
export const P2SH_OUTPUT_SIZE = 32;
export const P2ST_MIN_OUTPUT_SIZE = 33; // ??
export const VERSION_SIZE = 1;
export const LOCKTIME_SIZE = 4;

export const p2stOutputSize = (nexaAddress: string): number => {
  const bufferLength = nexcore.Address.decodeNexaAddress(nexaAddress).hashBuffer.length;
  const pushSize = bufferLength > 75 ? 2 : 1; // TODO: revisit
  return 1 + 8 + pushSize + bufferLength;
};
